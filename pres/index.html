<!DOCTYPE html>
<html>
  <head>
    <title>PSA</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="core/fonts/mono.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/animate.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/cinescript.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_core.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/mermaid.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/gitgraph.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_ensiie.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/katex.css"> 
</head>
    <body>

    <textarea id="source" readonly>
      
        class: titlepage

        .title[Projet de PSA - Un solveur 2D pour l'équation de Schrödinger non relativiste dépendante du temps :notebook:]
      
        .subtitle[F. Fiscus, A. Coreau, L. Darfeuille - ENSIIE - 2023]

        
        
        .footnote[
        `Projet de PSA - Un solveur 2D pour l'équation de Schrödinger` - F. Fiscus, A. Coreau, L. Darfeuille - ENSIIE - 2023 - :book:
        ]

        ---

        layout: true
        class: animated fadeIn numbers

        .footnote[
        `Projet de PSA - Un solveur 2D pour l'équation de Schrödinger` - F. Fiscus, A. Coreau, L. Darfeuille - 2023 - :book:
        ]        
                  
        ---

        
        # Table of contents

        <br>
        1. Où on en était
        <br> <br>
        2. Pre/Post-processing 
          
          A. Backend
          <br>
          B. Frontend
          <br>
          C. Usecases
        <br> <br>
        3. Solver, la suite
          
          A. Les changements
          <br>
          B. Les tests unitaires
          <br>
          C. Les résultats
        <br> <br>
        4. Conclusion
        














        ---

        class:titlepage

        .title[I. Où on en était]


        ---

        class:middle

        #I. Où on en était

        ## But du projet
        * Réaliser un solver 2D-FD de l'équation de Schrödinger non relativiste dépendant du temps à deux dimensions

        ## Équation de Schrödinger non relativiste dépendante du temps
        `$$i\hbar\frac{\partial}{\partial t}\psi(x,y,t) = \hat{\mathcal{H}}_{(x,y)}\psi(x,y,t)$$`

        ## Sous objectifs du projet qui étaient réalisés

        :+: Pre-processing quasi-terminé (JSON, Python, préparer la base de donnée)

        :+: Écrire le solver avec différentes méthodes : explicite, implicite et Crank-Nicolson

        :+: Implémenter un méchanisme de redémarrage automatique

        :+: Stocker les résultats dans une base de données
        
        </br>

        ## Sous objectifs du projet qui restaient à faire

        * Réaliser les tests unitaires du solver
        * Visualiser l'évolution au cours du temps d'un paquet d'onde
        * Obtenir des informations de suivi en temps réel
        * Calculer les résultats pour quelques applications physiques







        ---

        class:titlepage

        .title[II. Pre/Post-processing]


        ---

        class:middle

        #A. Organisation des fichiers

        .row.w60[
        .tree[
        src
        * psa_solver_module
            * cpp
              * solver.cpp
              * solver.h
            * init_and_utilities.py
            * preprocessing.py
        * solver.py
        * postprocessing.py
        ]
        

        .column.shadow.w40[![](images/processing_orga.png)]
        
        ]

        **flèche** :arrow_right: **utilise**
        

        ---

        class:middle

        #A. Backend

        ## init_and_utilities.py

        * provides Initialization class
          * stores the initial parameters
          * saves them in database
          * provides functions to compute the initial state
        * provides mongoInfo class
          * stores database information
        * provides functions to access and read/write stuff to the database
          * load psi matrices
          * save psi matrices
          * load parameters
          * ...
        * provides uilities functions to convert numpy matrix to binary bson object
        * ...

        ## preprocessing.py

        * provides function to cleanup database
        * provides the autorestart mechanism












        ---

        class:middle

        #B. Frontend

        ## solver.py

        A json file is used as input for the initial state of the solver and the account
        and database name for storing the data in a mongodb database (local or remote).
        The solver then uses it to compute the solutions : 
        ```bash
        ./solver.py jsonfile
        ```

        ### .hcenter[Json file example : young's slits experiment]

        ```json
        * "method": "ctcs",
        * "image": {
            "is_path_defined": true,	
            "path_potential_init": "./test_images/V0_young.png",
            "vmin": 0,
            "vmax": 30 },
        * "parameters": {
            "xmin": -10,
            "xmax": 10,
            "timestep": 0.005,
            "totaltime": 2.0,
            "x_num_point": 101,
            "ymin": -10,
            "ymax": 10,
            "y_num_point": 101,
            "m": 1,
            "h_bar": 1},
        * "psi_function": {
            "gaussian": [ {
              "init_velocity_x": 5,
              "init_velocity_y": 0,
              "x0": -8,
              "y0": 0,
              "width": 2,
              "norm_constant": 0.3989422804,
              "weight": 1}] }    
        ```


        
        ---

        class:middle

        #B. Frontend

        ## postprocessing.py
        
        Handles monitoring, preprocessing visualization and postprocessing visualization.
        ```bash
        ./postprocessing.py user_option [ jsonfile | fps ]
        ```        
        ### User options
        
        - 'e' to generate the graph of the current normalisation error since the beginning of computation
        - 'i' to generate a vtk image of the initial state of the solver
        - 'l' to generate the vtk image of the last psi computed by the solver
        - 'a' to generate the vtk images of all the psis computed by solver so far
        - 'm' to modify the parameters used by an ongoing calculation that has been stopped (dangerous, beware !)
        - 'ao' to generate the vtk images of all the psis computed by solver so far using mutliprocessing (faster)
        - 'anim' to generate the minimum amount of vtk images of the psis to have an animation of the whole thing so far 
        - 'animo' like 'anim' but with multiprocessing (faster)

        'jsonfile' typically used with the 'm' or 'i' option and 'fps' used with the 'anim' or 'animo' option.








        ---

        class:middle

        #B. Frontend

        ## postprocessing.py : example

        <pre><code>./postprocessing.py i ../json/young_slits.json 
        DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 2.000000s | <font color="#19CB00">psi vtk image generated</font>
        </code></pre>

        <br/>
        .hcenter[![**Visualization with Paraview**](../src/vtk_animations/young_init.png)]

        ---


        #C. Usecases

        ## Monitoring example (tunnel effect)

        We launch the solver on a supercomputer:
        <pre><code>./solver.py ../json/tunneling.json 
          DEBUG - different json hash, cleaning up database and saving new hash
          DEBUG - parameters saved, id: 6461009c212b2bb2b2e4aa20
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="FFFF00">0%</font>
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="FFFF00">5%</font>
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="FFFF00">10%</font>
          ...
        </code></pre>

        And at the same time on a local computer:
        <pre><code>./postprocessing.py l
        DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | <font color="#19CB00">latest psi vtk image generated</font>
        </code></pre>

        Finally we can visualize it with paraview:

        .hcenter.shadow.w60[![**Visualization with Paraview**](../src/vtk_animations/tunnel_monit.png)]



        ---


        #C. Usecases

        ## Monitoring example (tunnel effect)

        We launch the solver on a supercomputer:
        <pre><code>./solver.py ../json/tunneling.json 
          DEBUG - different json hash, cleaning up database and saving new hash
          DEBUG - parameters saved, id: 6461009c212b2bb2b2e4aa20
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="FFFF00">0%</font>
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="FFFF00">5%</font>
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="FFFF00">10%</font>
          ...
        </code></pre>

        And at the same time on a local computer we can also see the normalization error:
        ```bash
        ./postprocessing.py e
        ```
        This command opens up a matplotlib window showing the error and refreshing the data every 0.05 seconds:
        
        .hcenter.shadow.w38[![](images/norm_error_tunnel.png)]





        ---


        #C. Usecases

        ## Autorestart
        
        Let's simulate the crash of the solver's process:

        <pre><code>./solver.py ../json/2dHO_solutions.json 
          DEBUG - different json hash, cleaning up database and saving new hash
          DEBUG - parameters saved, id: 64610f8a212b2bb2b2e4db16
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">0%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">1%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">2%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">3%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">4%</font>
          ...
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font  color="FFFF00">22%</font>
          ^C
          KeyboardInterrupt

        </code></pre>

        Now if I relaunch the solver with exactly the same json input file:
        <pre><code>./solver.py ../json/2dHO_solutions.json 
          DEBUG - same json hash, proceeding to load psi0 from database
          DEBUG - timestep loaded: 2.213500 time total: 10.000000
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">22%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">23%</font> 
          ...
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | progress: <font color="FFFF00">99%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | <font color="19CB00">finished solving stuff</font>
        </code></pre>
        
        Now I want to generate the vtk images to have a 30 fps animation of the simulation:
        <pre><code>./postprocessing.py animo 30
          DEBUG - using fps = 30          
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | 0/10s vtk images processed : <font color="#FFFF00">0%</font>
          ...
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | 10/10s vtk images processed : <font color="#19CB00">100%</font>
          DEBUG - method: btcs | timestep: 0.500000ms | totaltime: 10.000000s | <font color="#19CB00">vtk images have been processed</font>
          DEBUG - time taken: <font color="#0000FF">11.531461 seconds</font>
        </code></pre>







        ---


        #C. Usecases

        ## Autorestart (bis)

        Using paraview to generate all the png and ffmpeg to generate from those pngs the webm video, we get:

        .hcenter.shadow[
        <video controls="yes" loop="yes" preload="auto" width="100%" height="auto" data-setup="{}" loop>
        <source src="../src/vtk_animations/2dHO.webm" type="video/webm" />
        </video>
        ]
        


        ---


        #C. Usecases

        ## Start a new calculation from an existing result

        Let's say we launched a calculation for a simulation of 5 seconds
        but we suddently want to do the same calculation but for 10 seconds instead of 5.
        
        .hcenter[**launching solver with 5 seconds as parameter**
        
        <pre><code>./solver.py ../json/tunneling.json 
        DEBUG - different json hash, cleaning up database and saving new hash
        DEBUG - parameters saved, id: 64611e15212b2bb2b2e5cb3d
        DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 5.000000s | progress: <font color="#FFFF00">0%</font>
        ...
        DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 5.000000s | progress: <font color="#19CB00">100%</font>
        DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 5.000000s | <font color="#19CB00">finished solving stuff</font>
        </code></pre>
        
        ]

        .hcenter[**launching solver with 10 seconds starting from the previous result**
          
        <pre><code>./postprocessing.py m ../json/tunneling.json 
          DEBUG - parameters saved, id: 64611e15212b2bb2b2e5cb3d
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | <font color="#19CB00">Saved new jsonhash, the calculation is good to go !</font>
        </code></pre>
        ]

        .hcenter[**relaunching the solver with the exact same command as last time**
        <pre><code>./solver.py ../json/tunneling.json 
          DEBUG - same json hash, proceeding to load psi0 from database
          DEBUG - timestep loaded: 5.005000 time total: 10.000000
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="#FFFF00">50%</font>
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="#FFFF00">55%</font>
          ...
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | progress: <font color="#FFFF00">95%</font>
          DEBUG - method: ctcs | timestep: 5.000000ms | totaltime: 10.000000s | <font color="#19CB00">finished solving stuff</font>
        </code></pre>
        ]

        .row.w10[Note: if we relaunch the solver :arrow_right:
        <pre><code>./solver.py ../json/tunneling.json 
          DEBUG - same json hash, proceeding to load psi0 from database
          DEBUG - timestep loaded: 10.000000 time total: 10.000000
          DEBUG - Simulation finished, stopping simulation
        </code></pre>
        ]


        ---

        # A. Les changements

        **solver.cpp**

        ```c++
        while (error >= epsilon) {
        
        *  tmpr = (psi.rpart - dt * 0.5 * (potentiel % (psi.ipart + g_ipart)
        *  + const_psi_dx * (arma::shift(psi.ipart,-1) + arma::shift(psi.ipart,+1) + arma::shift(g_ipart,-1) + arma::shift(g_ipart,+1))
        *  + const_psi_dy * (arma::shift(psi.ipart,-1,1) + arma::shift(psi.ipart,+1,1) + arma::shift(g_ipart,-1,1) + arma::shift(g_ipart,+1,1))));
  
          next_g_rpart.submat(1,1, n-2, n-2) = tmpr.submat(1,1, n-2, n-2);
  
        *  tmpi = (psi.ipart + dt * 0.5 * (potentiel % (psi.rpart + g_rpart)
        *  + const_psi_dx * (arma::shift(psi.rpart,-1) + arma::shift(psi.rpart,+1) + arma::shift(g_rpart,-1) + arma::shift(g_rpart,+1))
        *  + const_psi_dy * (arma::shift(psi.rpart,-1,1) + arma::shift(psi.rpart,+1,1) + arma::shift(g_rpart,-1,1) + arma::shift(g_rpart,+1,1))));
          
          next_g_ipart.submat(1,1, n-2, n-2) = tmpi.submat(1,1, n-2, n-2); 
  
          error = abs((next_g_ipart % next_g_ipart + next_g_rpart % next_g_rpart).max()-(g_ipart % g_ipart + g_rpart % g_rpart).max()); 
  
          g_rpart.submat(1,1, n-2, n-2) = next_g_rpart.submat(1,1,n-2,n-2);
          g_ipart.submat(1,1, n-2, n-2) = next_g_ipart.submat(1,1,n-2,n-2);
  
          }```
        <br> 
        :arrow_right: Correction du calcul pour la méthode CTCS

        ---

        # A. Les changements

        **solver.h**

        ```c++
        class Solver
        {
        public:
        
        ...

        *psi_mat add_padding(psi_mat psi0);
        *arma::mat padded_matrix(arma::mat m);
        
        *double norm();

        };
        ```
        :arrow_right: Ajout des fonctions permettant le rajout de zéro autour des matrices <br> 
        :arrow_right: Ajout de la fonction permettant le calcul de la normalisation
       
        **solver.cpp**

        ```c++
        arma::mat Solver::padded_matrix(arma::mat m) {
          int n = m.n_cols;
          arma::mat res(n+2,n+2);
          res.submat(1,1,n,n) = m;
          return res;
          }
      
        psi_mat Solver::add_padding(psi_mat psi0){
          int n = psi0.rpart.n_cols;
          arma::mat new_rpart(n+2,n+2);
          arma::mat new_ipart(n+2,n+2);
      
          new_rpart.submat(1,1,n,n) = psi0.rpart;
          new_ipart.submat(1,1,n,n) = psi0.ipart;
      
          psi_mat new_psi(new_rpart,new_ipart);
          return new_psi;
        }```

        ---

        # B. Les tests unitaires

        **test.h**

        ```c++
        class Test : public CxxTest::TestSuite {
        public:
          void test0(void) {
            arma::mat _V0(10,10);
            arma::mat rpart(10,10);
            arma::mat ipart(10,10);
            arma::mat old_rpart,old_ipart;
            rpart.ones();
            ipart.zeros();
            _V0.zeros();    

            psi_mat psi0(rpart,ipart);

            Solver solver(_V0, psi0,1,1,'f',1,1,1);

            old_rpart = solver.psi.rpart;
            old_ipart = solver.psi.ipart;

            arma::mat new_ipart = {
                        {0,0,0,0,0,0,0,0,0,0,0,0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0,0,0,0,0,0,0,0,0,0,0,0}};

            solver.psi_dt_FTCS();
            TS_ASSERT((solver.psi.rpart - old_rpart).is_zero());
            TS_ASSERT((solver.psi.ipart - new_ipart).is_zero());};
        ```
        :arrow_right: Test pour FTCS, mais la logique est similaire pour les autres méthodes.
        ---
        # B. Les tests unitaires
        <br> <br>
        **test.h**

        ```c++
        ...

        void test3(void) {
          arma::mat _V0(10,10);
          arma::mat rpart(10,10);
          arma::mat ipart(10,10);
  
          arma::mat old_rpart,old_ipart;
  
          rpart.ones();
          ipart.zeros();
          _V0.zeros();    
  
          psi_mat psi0(rpart,ipart);
  
          Solver solver(_V0, psi0,1,1,'f',1,1,1);
          TS_ASSERT_EQUALS(solver.norm(), 100);
        };

        ...
        ```
        :arrow_right: Test pour la normalisation
        ---

        # B. Les tests unitaires

        **test.h**

        ```c++
        ...
        void test4(void) {
          arma::mat _V0(10,10);
          arma::mat rpart(10,10);
          arma::mat ipart(10,10);
  
          arma::mat old_rpart,old_ipart;
  
          rpart.ones();
          ipart.zeros();
          _V0.zeros();    
  
          psi_mat psi0(rpart,ipart);
  
          Solver solver(_V0, psi0,1,1,'f',1,1,1);
          arma::mat m1 = {
              {1,2,3,4,5},
              {1,2,3,4,5},
              {1,2,3,4,5},
              {1,2,3,4,5},
              {1,2,3,4,5}
          };
          arma::mat r1 = {
              {0,0,0,0,0,0,0},
              {0,1,2,3,4,5,0},
              {0,1,2,3,4,5,0},
              {0,1,2,3,4,5,0},
              {0,1,2,3,4,5,0},
              {0,1,2,3,4,5,0},
              {0,0,0,0,0,0,0}
          };
          TS_ASSERT((solver.padded_matrix(m1) - r1).is_zero());
        };
        

        
        ```
        :arrow_right: Test pour les fonctions de padding
        ---
        # C. Les résultats

        
        **Fentes de Young**  `\( \psi_0(x,y) = \frac{1}{\sqrt{2\pi}}.e^{-\left[\frac{(x + 8)^2+y^2}{4}\right]}.e^{i\left(5x\right)} \)`
        
        .hcenter.w80[
        <video controls="yes" loop="yes" preload="auto" width="100%" height="auto" data-setup="{}" loop>
        <source src="../src/vtk_animations/ctcs_young.webm" type="video/webm" />
        </video>
        ]

        ---
        # C. Les résultats

        <br/>
        **Effet tunnel**  `\( \psi_0(x,y) = \frac{1}{\sqrt{2\pi}}.e^{-\left[\frac{(x + 8)^2+y^2}{4}\right]}.e^{i\left(2x\right)} \)`
        <br/><br/>
        .hcenter.w80[
        <video controls="yes" loop="yes" preload="auto" width="100%" height="auto" data-setup="{}" loop>
        <source src="../src/vtk_animations/tunnel_ctcs.webm" type="video/webm" />
        </video>
        ]



        ---

        class:middle

        # Conclusion

        ## But du projet
        * Réaliser un solver 2D-FD de l'équation de Schrödinger non relativiste dépendant du temps à deux dimensions

        </br>

        ## Sous objectifs du projet réalisés

        :+: Les use cases

        :+: Écrire le solver avec différentes méthodes : ftcs, btcs et ctcs

        :+: Monitoring de l'erreur de normalisation et logs de progression

        :+: Interface utilisable par l'utilisateur lambda avec readme

        :+: Documentation python et C++

        :+: Tests unitaires python (cf. présentation précédente) et C++.
        
        </br>

        ## Améliorations possibles

        * Plus d'informations donné par le monitoring (sans trop de verbosité)
        * Implémentation d'autres schémas numériques
        * Fournir plus d'exemple de cas physique
        * Élargir le solver à d'autres équations que celle utilisée ici
        * etc... (les possibilités sont infinis !)























        
        </textarea>
        
            <script src="core/javascript/remark.js"></script>
            <script src="core/javascript/katex.min.js"></script>
            <script src="core/javascript/auto-render.min.js"></script>
            <script src="core/javascript/emojify.js"></script>
            <script src="core/javascript/mermaid.js"></script>
            <script src="core/javascript/term.js"></script>
            <script src="core/javascript/jquery-2.1.1.min.js"></script>
            <script src="core/javascript/extend-jquery.js"></script>
            <script src="core/javascript/cinescript.js"></script>
            <script src="core/javascript/gitgraph.js"></script>
            <script>
        
            // === Remark.js initialization ===
            var slideshow = remark.create({
              highlightStyle: 'monokai',
              countIncrementalSlides: false,
              highlightLines: true
            });
        
            // === Mermaid.js initialization ===
            mermaid.initialize({
              startOnLoad: false,
              cloneCssStyles: false,
              flowchart:{
                height: 50
              },
              sequenceDiagram:{
                width: 110,
                height: 30
              }
            });
        
            function initMermaid(s) {
              var diagrams = document.querySelectorAll('.mermaid');
              var i;
              for(i=0;i<diagrams.length;i++){
                if(diagrams[i].offsetWidth>0){
                  mermaid.init(undefined, diagrams[i]);
                }
              }
            }
        
            slideshow.on('afterShowSlide', initMermaid);
            initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);
        
            
            // === Emojify.js initialization ===
            emojify.run();
        
            // === Cinescript initialization ===
            $(document).ready(init_cinescripts);
        
            renderMathInElement(document.body,{delimiters: [{left: "$$", right: "$$", display: true}, {left: "\\(", right: "\\)", display: false}], ignoredTags: [] });
        
            </script>

    </body>
</html>