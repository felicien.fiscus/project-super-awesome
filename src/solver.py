#!/usr/bin/env python3
"""!
    @file solver.py
    @brief usage: ./solver.py jsonfile [-v]
        where jsonfile is the json containing the parameters of the initial state of the solver
        and where -v is an optional parameter to print normalization error on terminal.
"""

import psa_solver as psa
from psa_solver import init_and_utilities as iu
from psa_solver import preprocessing as prep
from colorama import Fore
import sys



def main():
    jsonfile = '../json/file_user.json'
    print_error = False
    if len(sys.argv) > 1:
        if sys.argv[1] == '-clean':    
            mongoInfo = iu.mongoInfo(jsonfile)
            mongoClient = iu.open_connection(mongoInfo.user, mongoInfo.pwd, mongoInfo.ip, mongoInfo.db)
            prep.cleanup_database(mongoClient, mongoInfo.db)
            mongoClient.close()
            return

        jsonfile = sys.argv[1]
        if len(sys.argv) > 2 and sys.argv[2] == "-v":
            print_error = True


    mongoInfo = iu.mongoInfo(jsonfile)
    solver_init, accu = prep.init_solver(jsonfile, mongoInfo)
    debug_info = 'method: %s | timestep: %fms | totaltime: %fs | ' % (solver_init.method, solver_init.dt*1e3, solver_init.total_time)
    solver = psa.Solver(solver_init.V0, solver_init.psi0, solver_init.h_bar, solver_init.m, solver_init.method[0], solver_init.dx, solver_init.dy, solver_init.dt)
    mongoClient = iu.open_connection(mongoInfo.user, mongoInfo.pwd, mongoInfo.ip, mongoInfo.db)
    iu.save_psi0(solver_init.psi0, accu, mongoClient, mongoInfo.db)
    p = 0
    error_init = solver.norm()
    while accu <= solver_init.total_time:
        accu += solver_init.dt
        psi = solver.calcul_psi_t_plus_dt()
        iu.save_psi0(psi, accu, mongoClient, mongoInfo.db)
        if print_error:
            iu.logger.debug(debug_info + 'normal error: %f' % (solver.norm() - error_init))
        iu.save_error(solver.norm() - error_init, accu, mongoClient, mongoInfo.db)
        if  p % 100 == 0:
            iu.logger.debug(debug_info + 'progress: ' + Fore.YELLOW + ('%d%%' % (100.0 * accu / solver_init.total_time)) + Fore.RESET)
        p += 1
    
    iu.logger.debug(debug_info + Fore.GREEN + 'finished solving stuff' + Fore.RESET)
    mongoClient.close()


if __name__ == '__main__':
    main()