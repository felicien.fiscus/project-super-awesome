#include <cxxtest/TestSuite.h>
#include <armadillo>
#include <iostream>
#include "../solver.h"

class Test : public CxxTest::TestSuite
{
public:
    void test0(void) {
        arma::mat _V0(10,10);
        arma::mat rpart(10,10);
        arma::mat ipart(10,10);

        arma::mat old_rpart,old_ipart;

        rpart.ones();
        ipart.zeros();
        _V0.zeros();    

        psi_mat psi0(rpart,ipart);

        Solver solver(_V0, psi0,1,1,'f',1,1,1);

        old_rpart = solver.psi.rpart;
        old_ipart = solver.psi.ipart;

        arma::mat new_ipart = {
                        {0,0,0,0,0,0,0,0,0,0,0,0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0,0,0,0,0,0,0,0,0,0,0,0}};

        solver.psi_dt_FTCS();
            
        TS_ASSERT((solver.psi.rpart - old_rpart).is_zero());
        TS_ASSERT((solver.psi.ipart - new_ipart).is_zero());
    };

    void test1(void) {
        arma::mat _V0(10,10);
        arma::mat rpart(10,10);
        arma::mat ipart(10,10);

        arma::mat old_rpart,old_ipart;

        rpart.ones();
        ipart.zeros();
        _V0.zeros();    

        psi_mat psi0(rpart,ipart);

        Solver solver(_V0, psi0,1,1,'b',1,1,1);

        old_rpart = solver.psi.rpart;
        old_ipart = solver.psi.ipart;

        arma::mat new_ipart = {
                        {0,0,0,0,0,0,0,0,0,0,0,0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0,0,0,0,0,0,0,0,0,0,0,0}};

        solver.psi_dt_FTCS();
            
        TS_ASSERT((solver.psi.rpart - old_rpart).is_zero());
        TS_ASSERT((solver.psi.ipart - new_ipart).is_zero());
    };

    void test2(void) {
        arma::mat _V0(10,10);
        arma::mat rpart(10,10);
        arma::mat ipart(10,10);

        arma::mat old_rpart,old_ipart;

        rpart.ones();
        ipart.zeros();
        _V0.zeros();    

        psi_mat psi0(rpart,ipart);

        Solver solver(_V0, psi0,1,1,'c',1,1,1);

        old_rpart = solver.psi.rpart;
        old_ipart = solver.psi.ipart;

        arma::mat new_ipart = {
                        {0,0,0,0,0,0,0,0,0,0,0,0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -.5, 0, 0, 0, 0, 0, 0, 0, 0, -.5, 0},
                        {0, -1, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, -1, 0},
                        {0,0,0,0,0,0,0,0,0,0,0,0}};

        solver.psi_dt_FTCS();
            
        TS_ASSERT((solver.psi.rpart - old_rpart).is_zero());
        TS_ASSERT((solver.psi.ipart - new_ipart).is_zero());
    };

    void test3(void) {
        arma::mat _V0(10,10);
        arma::mat rpart(10,10);
        arma::mat ipart(10,10);

        arma::mat old_rpart,old_ipart;

        rpart.ones();
        ipart.zeros();
        _V0.zeros();    

        psi_mat psi0(rpart,ipart);

        Solver solver(_V0, psi0,1,1,'f',1,1,1);
        TS_ASSERT_EQUALS(solver.norm(), 100);
    };

    void test4(void) {
        arma::mat _V0(10,10);
        arma::mat rpart(10,10);
        arma::mat ipart(10,10);

        arma::mat old_rpart,old_ipart;

        rpart.ones();
        ipart.zeros();
        _V0.zeros();    

        psi_mat psi0(rpart,ipart);

        Solver solver(_V0, psi0,1,1,'f',1,1,1);
        arma::mat m1 = {
            {1,2,3,4,5},
            {1,2,3,4,5},
            {1,2,3,4,5},
            {1,2,3,4,5},
            {1,2,3,4,5}
        };
        arma::mat r1 = {
            {0,0,0,0,0,0,0},
            {0,1,2,3,4,5,0},
            {0,1,2,3,4,5,0},
            {0,1,2,3,4,5,0},
            {0,1,2,3,4,5,0},
            {0,1,2,3,4,5,0},
            {0,0,0,0,0,0,0}
        };
        TS_ASSERT((solver.padded_matrix(m1) - r1).is_zero());
    };
};