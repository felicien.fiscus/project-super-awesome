/*! \file solver.cpp
 * Ce fichier est l'implémentation de solver.h
 * On y définit l'initialiseur de la classe Solver, ainsi que les différentes méthodes de calcul de psi de t+dt 
 */

#ifndef SOLVER_CPP
#define SOLVER_CPP
#include "solver.h"
#include <armadillo>

/*
 * Définition de l'ininialisateur de la classe solveur.
 * On y prend la matrice représentant le potentiel et celles représentants l'état initial de la fonction psi
 * On ajoute à ces matrice un contour de zéro pour faciliter les calculs. Ce dernier n'influe pas sur les calculs ou là remontée de l'information à python
 * Les autres objets de la classe sont soit des paramètres, soit des matrices que nous initialisons pour ne pas avoir à le faire à chaque calcul de psi de t+dt
 * 
 * @param _V0 la matrice du potentiel
 * @param psi0 les matrices (partie réelle et imaginaire) de l'état initial de la fonction psi
 * @param _h_bar la valeur à attribuer à h_bar la constante de plank réduite
 * @param _m la masse de la particule
 * @param _method le char permettant d'identifier la méthode de calcul de psi de t+dt
 * @param _dx le pas d'espace sur l'axe x
 * @param _dy le pas d'espace sur l'axe y
 * @param _dt le pas de temps
 */
Solver::Solver(const arma::mat& _V0, psi_mat& psi0, double _h_bar, 
double _m, char _method, double _dx, double _dy, double _dt)
:V0(padded_matrix(_V0)), psi(add_padding(psi0)), h_bar(_h_bar), m(_m), method(_method), dx(_dx), dy(_dy), dt(_dt), tmpr(V0.n_cols,V0.n_cols), 
tmpi(V0.n_cols,V0.n_cols), g_rpart(V0.n_cols,V0.n_cols), g_ipart(V0.n_cols,V0.n_cols), next_g_rpart(V0.n_cols,V0.n_cols),
 next_g_ipart(V0.n_cols,V0.n_cols)
{
}

/*
 * La fonction permettant de calculer psi de t+dt. 
 * Elle fait appel à trois fonctions auxiliaires possible, qui sont déterminé en fonction du schéma de calcul choisi
 * 
 * En particulier nous avons implémenté les méthode de calcul FTCS, BTCS et CTCS (Crank-Nicolson)
 * Le résultat d'un tel calcul est ensuite réinjecté dans l'attribut "psi" de l'objet solver.
 * En plus de cela, on retourne le psi calculé.
*/
psi_mat Solver::calcul_psi_t_plus_dt()
{
    int n = psi.rpart.n_cols;
    
    switch (method) {
        case 'f': {
            psi_dt_FTCS();
            break;}
        case 'b': {
            psi_dt_BTCS();
            break;}
        case 'c': {
            psi_dt_CTCS();
            break;}
        default: 
            break;
    }
    psi_mat res(psi.rpart.submat(1,1,n-2,n-2),psi.ipart.submat(1,1,n-2,n-2));
    return res;
}

/*
 * Fonction permettant d'ajouter un contour de zéro à une matrice
 * 
 * @param m une matrice
 * @return une matrice identique entourée de zéros 
 */
arma::mat Solver::padded_matrix(arma::mat m) {
    int n = m.n_cols;
    arma::mat res(n+2,n+2);
    res.submat(1,1,n,n) = m;
    return res;
}


/*
 * Fonction permettant d'ajouter un contour de zero aux deux matrice d'une instance d'un objet psi_mat
 * 
 * @param psi0 une psi_mat
 * @return une psi_mat entourée de zéros
 */
psi_mat Solver::add_padding(psi_mat psi0){
    int n = psi0.rpart.n_cols;
    arma::mat new_rpart(n+2,n+2);
    arma::mat new_ipart(n+2,n+2);

    new_rpart.submat(1,1,n,n) = psi0.rpart;
    new_ipart.submat(1,1,n,n) = psi0.ipart;

    psi_mat new_psi(new_rpart,new_ipart);
    return new_psi;
}

/*
 * Procédure permettant de calculer psi de t+dt en utilisant la méthode FTCS
 */
void Solver::psi_dt_FTCS() {
    int n = psi.rpart.n_cols;

    arma::mat potentiel(n,n);

    double const_dx = h_bar / (m * dx * dx);
    double const_dy = h_bar / (m * dy * dy);

    arma::mat tmp(n,n);
    tmp.ones();

    double const_psi_dx = const_dx / 2;
    double const_psi_dy = const_dy / 2;

    potentiel = ((-1 / h_bar) * V0) - const_dx * tmp - const_dy * tmp;

    tmpr = (psi.rpart - dt * ((potentiel % psi.ipart)
        + const_psi_dx * (arma::shift(psi.ipart, -1) + arma::shift(psi.ipart,+1))
        + const_psi_dy * (arma::shift(psi.ipart, -1, 1) + arma::shift(psi.ipart, +1, 1))));

    psi.rpart.submat(1,1, n-2, n-2) = tmpr.submat(1,1, n-2, n-2);
    
    tmpi = (psi.ipart + dt * ((potentiel % psi.rpart)
        + const_psi_dx * (arma::shift(psi.rpart, -1) + arma::shift(psi.rpart,+1))
        + const_psi_dy * (arma::shift(psi.rpart, -1, 1) + arma::shift(psi.rpart, +1, 1))));

    psi.ipart.submat(1,1,n-2,n-2) = tmpi.submat(1,1, n-2, n-2);

    //clean();
}

/*
 * Procédure permettant de calculer psi de t+dt en utilisant la méthode BTCS
 */
void Solver::psi_dt_BTCS(){
    int n = psi.rpart.n_cols;
    arma::mat next_psi_rpart(n,n);
    arma::mat next_psi_ipart(n,n);

    arma::mat potentiel(n,n);

    double const_dx = h_bar / (m * dx * dx);
    double const_dy = h_bar / (m * dy * dy);

    arma::mat tmp(n,n);
    tmp.ones();

    double const_psi_dx = const_dx / 2;
    double const_psi_dy = const_dy / 2;

    potentiel = ((-1 / h_bar) * V0) - const_dx * tmp - const_dy * tmp;

    double error = 1.;
    double epsilon = 1e-14;
    

    tmpr = (psi.rpart - dt * ((potentiel % psi.ipart)
        + const_psi_dy * (arma::shift(psi.ipart, -1) + arma::shift(psi.ipart,+1))
        + const_psi_dy * (arma::shift(psi.ipart, -1, 1) + arma::shift(psi.ipart, +1, 1))));

    g_rpart.submat(1,1, n-2, n-2) = tmpr.submat(1,1, n-2, n-2);

    tmpi = (psi.ipart + dt * ((potentiel % psi.rpart)
        + const_psi_dx * (arma::shift(psi.rpart, -1) + arma::shift(psi.rpart,+1))
        + const_psi_dy * (arma::shift(psi.rpart, -1, 1) + arma::shift(psi.rpart, +1, 1))));
    
    g_ipart.submat(1,1,n-2,n-2) = tmpi.submat(1,1, n-2, n-2);   
    
    //clean();

    while (error >= epsilon) {
        
        tmpr = (psi.rpart - dt * ((potentiel % g_ipart) 
        + const_psi_dx * (arma::shift(g_ipart, -1) + arma::shift(g_ipart,+1))
        + const_psi_dy * (arma::shift(g_ipart, -1, 1) + arma::shift(g_ipart, +1, 1))));

        next_g_rpart.submat(1,1,n-2,n-2) = tmpr.submat(1, 1, n-2, n-2);

        tmpi = (psi.ipart + dt * ((potentiel % g_rpart)
        + const_psi_dx * (arma::shift(g_rpart, -1) + arma::shift(g_rpart,+1))
        + const_psi_dy * (arma::shift(g_rpart, -1, 1) + arma::shift(g_rpart, +1, 1))));
        
        next_g_ipart.submat(1,1,n-2,n-2) = tmpi.submat(1,1, n-2, n-2); 

        error = abs((next_g_ipart % next_g_ipart + next_g_rpart % next_g_rpart).max()-(g_ipart % g_ipart + g_rpart % g_rpart).max()); 

        g_rpart.submat(1,1, n-2, n-2) = next_g_rpart.submat(1,1,n-2,n-2);
        g_ipart.submat(1,1, n-2, n-2) = next_g_ipart.submat(1,1,n-2,n-2);

        //clean();
    }

    psi.rpart.submat(1,1, n-2, n-2) = next_g_rpart.submat(1,1,n-2,n-2);
    psi.ipart.submat(1,1, n-2, n-2) = next_g_ipart.submat(1,1,n-2,n-2);
}

/*
 * Procédure permettant de calculer psi de t+dt en utilisant la méthode CTCS
 */
void Solver::psi_dt_CTCS(){
    int n = psi.rpart.n_cols;
    arma::mat next_psi_rpart(n,n);
    arma::mat next_psi_ipart(n,n);

    arma::mat potentiel(n,n);

    double const_dx = h_bar / (m * dx * dx);
    double const_dy = h_bar / (m * dy * dy);

    arma::mat tmp(n,n);
    tmp.ones();

    double const_psi_dx = const_dx / 2;
    double const_psi_dy = const_dy / 2;

    potentiel = ((-1 / h_bar) * V0) - const_dx * tmp - const_dy * tmp;

    double error = 1.;
    double epsilon = 1e-14;

    tmpr = (psi.rpart - dt * ((potentiel % psi.ipart)
        + const_psi_dx * (arma::shift(psi.ipart, -1) + arma::shift(psi.ipart,+1))
        + const_psi_dy * (arma::shift(psi.ipart, -1, 1) + arma::shift(psi.ipart, +1, 1))));

    g_rpart.submat(1,1, n-2, n-2) = tmpr.submat(1,1, n-2, n-2);

    tmpi = (psi.ipart + dt * ((potentiel % psi.rpart)
        + const_psi_dx * (arma::shift(psi.rpart, -1) + arma::shift(psi.rpart,+1))
        + const_psi_dy * (arma::shift(psi.rpart, -1, 1) + arma::shift(psi.rpart, +1, 1))));
    
    g_ipart.submat(1,1,n-2,n-2) = tmpi.submat(1,1, n-2, n-2);   
    
    //clean();

    while (error >= epsilon) {
        
        tmpr = (psi.rpart - dt * 0.5 * (potentiel % (psi.ipart + g_ipart)
        + const_psi_dx * (arma::shift(psi.ipart,-1) + arma::shift(psi.ipart,+1) + arma::shift(g_ipart,-1) + arma::shift(g_ipart,+1))
        + const_psi_dy * (arma::shift(psi.ipart,-1,1) + arma::shift(psi.ipart,+1,1) + arma::shift(g_ipart,-1,1) + arma::shift(g_ipart,+1,1))));

        next_g_rpart.submat(1,1, n-2, n-2) = tmpr.submat(1,1, n-2, n-2);

        tmpi = (psi.ipart + dt * 0.5 * (potentiel % (psi.rpart + g_rpart)
        + const_psi_dx * (arma::shift(psi.rpart,-1) + arma::shift(psi.rpart,+1) + arma::shift(g_rpart,-1) + arma::shift(g_rpart,+1))
        + const_psi_dy * (arma::shift(psi.rpart,-1,1) + arma::shift(psi.rpart,+1,1) + arma::shift(g_rpart,-1,1) + arma::shift(g_rpart,+1,1))));
        
        next_g_ipart.submat(1,1, n-2, n-2) = tmpi.submat(1,1, n-2, n-2); 

        error = abs((next_g_ipart % next_g_ipart + next_g_rpart % next_g_rpart).max()-(g_ipart % g_ipart + g_rpart % g_rpart).max()); 

        g_rpart.submat(1,1, n-2, n-2) = next_g_rpart.submat(1,1,n-2,n-2);
        g_ipart.submat(1,1, n-2, n-2) = next_g_ipart.submat(1,1,n-2,n-2);

        //clean();
    }

    psi.rpart.submat(1,1, n-2, n-2) = next_g_rpart.submat(1,1,n-2,n-2);
    psi.ipart.submat(1,1, n-2, n-2) = next_g_ipart.submat(1,1,n-2,n-2);
}

/* 
 * Fonction permettant de calculer l'intégrale de la fonction psi
 * Nous utilisons cette fonction afin de calculer la norme de psi et vérifier que l'erreur que nous faisons 
 * avec les approximations numériques n'est pas trop élevé
 * 
 * @return un double, représentant le calcul de l'intégrale de psi sur tout l'espace
 */
double Solver::norm(){
    return arma::accu(((psi.rpart % psi.rpart + psi.ipart % psi.ipart) * dx * dy));
}

#endif //SOLVER_CPP