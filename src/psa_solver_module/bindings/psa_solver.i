%module psa_solver

%{
#define SWIG_FILE_WITH_INIT
#include "../cpp/solver.h"
%}

%include armanpy.i
%include "../cpp/solver.h"
%pythoncode
%{
    import init_and_utilities
    import preprocessing
%}