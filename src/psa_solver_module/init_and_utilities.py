"""!
    @file init_and_utilities.py
    @brief backend functions for initialization and database access
"""

from pymongo import MongoClient
import pymongo.errors
import json
import numpy as np
import bson
import pickle
from PIL import Image
from scipy import special as sp
import math
import psa_solver
import logging

logger = logging.getLogger('psa_logger')
logger.setLevel(logging.DEBUG)
fh = logging.StreamHandler()
fh_formatter = logging.Formatter('%(levelname)s - %(message)s')
fh.setFormatter(fh_formatter)
logger.addHandler(fh)

processing_collection = 'processing'
params_collection = 'parameters'
norm_error_collection = 'normalization'

def get_potential_from_image(path_to_image, bound_min, bound_max):
    """!
    @brief convert an image (greyscale) to a potential V(x, y) matrix with x going from 0 to the image's height 
        and y going from 0 to the image's width.
    @param path_to_image the path to the image file
    @param bound_min minimum value to be taken by the potential V(x, y) -> corresponds to a completely black pixel
    @param bound_max maximum value to be taken by the potential V(x, y) -> corresponds to a completely white pixel

    @return the potential V(x, y) as a numpy matrix of size image's width x image's height
    """
    if bound_min > bound_max:
        logger.error("error: the minimum potential bound is higher than the maximum potential bound\n")
        return []
    with Image.open(path_to_image) as image:
        #first get a grey scale image where each pixel goes from 0 to 255
        potential = np.asarray(image.convert("L").getdata(),dtype=np.double, order='F')
        size = (image.height, image.width)
        #then convert this so that each pixel goes from bound_min to bound_max
        potential = bound_min + (potential / 255.0) * (bound_max - bound_min)
        potential = np.reshape(potential, size, order='F')
        return potential

def test_getpotentialfromimage():
    """!
    @brief unit test for get_potential_from_image

    """
    epsilon = 1e-10
    bounds_peculiar = (0, 100)
    bounds_normal = (0,255)
    wanted_outputs = (np.mat([[0,100.0,0],[0.,100.0,100.0]]), np.mat([[0,255.0,0],[0.0,255.0,255.0]]), np.mat([[75.294117647, 100], [0, 100], [100, 0]])
    , np.mat([[192.0, 255], [0, 255], [255.0, 0]]))
    outputs = (get_potential_from_image('test_images/image_output1.png', bounds_peculiar[0], bounds_peculiar[1])
        , get_potential_from_image('test_images/image_output1.png', bounds_normal[0], bounds_normal[1])
        , get_potential_from_image('test_images/image_output2.png', bounds_peculiar[0], bounds_peculiar[1])
        , get_potential_from_image('test_images/image_output2.png', bounds_normal[0], bounds_normal[1]))
    for i in range(len(outputs)):
        assert(np.max(np.absolute(outputs[i] - wanted_outputs[i])) < epsilon)

##
#   @brief runs the unit tests of this file
#
def run_tests():
    test_getpotentialfromimage()


class mongoInfo:
    """!
    @brief object to hold mongodb information (user, password, ip address and database name) 
    """
    def __init__(self, jsonfile='../json/start_file.json'):
        """!
            @brief initializing mongoInfo with default values: user0, pwduser0, 127.0.0.1, psa_db
        """
        self.user = 'user0'
        self.pwd = 'pwduser0'
        self.ip = '127.0.0.1'
        self.db = 'psa_db'
        self.init_via_json(jsonfile)

    def init_via_json(self, jsonfile):
        """!
        @brief init the mongodb infos with a json file
        @param jsonfile the file containing the user, password, ip address and database information 
        """
        with open(jsonfile, 'r') as jfile:
            mjson = json.load(jfile)
            if 'mongodb' in mjson:
                json_db = mjson['mongodb']
                if 'user' in json_db:
                    self.user = json_db['user']
                if 'password' in json_db:
                    self.pwd = json_db['password']
                if 'ip_address' in json_db:
                    self.ip = json_db['ip_address']
                if 'database' in json_db:
                    self.db = json_db['database']


def open_connection(user, pwd, ip, database):
    """!
    @brief opens up a connection to a mongodb database
    @param user the user to connect in the database as
    @param pwd the password user by the user we are connecting as
    @param ip the address to the mongodb server running
    @param database the database we want to connect to
    """
    return MongoClient("mongodb://%s:%s@%s/%s" % (user, pwd, ip, database))


def numpy_to_binary(np_array):
    """!
    @brief converts a numpy array to its correct binary representation using bson and pickle
    @param np_array the nunmpy array to be converted
    @return the converted numpy array, that is a bson Binary object
    """
    return bson.Binary(pickle.dumps(np_array, protocol=2))

def binary_to_numpy(np_binary):
    """!
        @brief converts a binary representation of a numpy array to a numpy array using pickle
        @param np_binary the binary to be converted
        @return the converted binary numpy array
    """
    return np.array(pickle.loads(np_binary), order='F')


def load_param(param_name, mongoClient, database):
    """!
        @brief loads a parameter from the database and returns its value
        @param param_name the name of the parameter to load, can only be one of : 
            - V0
            - method
            - xmin
            - xmax
            - ymin
            - ymax
            - xnum
            - ynum
            - m
            - h_bar
            - totaltime
            - timestep
        @param mongoClient the client to mongoDB database used to load the data
        @param database where to load the parameters
        @return the parameter associated to the param_name, loaded from the database
    """
    try:
        param = mongoClient[database][params_collection].find_one({param_name: {"$exists": True}})[param_name]
        if param_name == 'V0':
            return binary_to_numpy(param)
        else:
            return param
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))



def save_psi0(psi0, timestep, mongoClient, database):
    """!
    @brief saves psi0 to mongodb database
    @param psi0 the data to be saved
    @param timestep the corresponding timestep
    @param mongoClient the client to access the mongodb database
    @param database the database where we want to store psi0
    """
    try:
        binpsi0_rpart = numpy_to_binary(psi0.rpart)
        binpsi0_ipart = numpy_to_binary(psi0.ipart)
        mongoClient[database][processing_collection].insert_one({'rpart': True, 'data': binpsi0_rpart,
            'timestep': timestep})
        mongoClient[database][processing_collection].insert_one({'ipart': True, 'data': binpsi0_ipart,
            'timestep': timestep})
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))


def save_error(norm_error, timestep, mongoClient, database):
    """!
    @brief saves the normalization error to mongodb database
    @param norm_error the data to be saved
    @param timestep the corresponding timestep
    @param mongoClient the client to access the mongodb database
    @param database the database where we want to store psi0
    """
    try:
        mongoClient[database][norm_error_collection].insert_one({'timestep': timestep, 'norm_error': norm_error})
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))

def load_all_errors(mongoClient, database, time_range = [-1, -1]):    
    """!
        @brief loads the collection of normalization errors from the database
        @param mongoClient the mongodb client to access the database
        @param database the database where to find the normalization error
        @param time_range if left as default ([-1, -1]) it loads all the errors but otherwise it loads the errors that have a timestep within these bounds
        @return (errors, timesteps) , sorted in increasing timestep order as two numpy arrays, if no mongodb error has occured, None otherwise
    """
    try:
        if time_range[0] == -1 and time_range[1] == -1:
            values = list(mongoClient[database][norm_error_collection].find({'norm_error':{"$exists": True}}).sort('timestep', 1))
            return np.array(list(map(lambda x : x['norm_error'], (values)))), np.array(list(map(lambda x : x['timestep'], (values))))
    
        values = list(mongoClient[database][norm_error_collection].find({'norm_error':{"$exists": True}, 'timestep': {"$gt": time_range[0], "$lt": time_range[1]}}).sort('timestep', 1))        
        return np.array(list(map(lambda x : x['norm_error'], (values)))), np.array(list(map(lambda x : x['timestep'], (values))))
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))
        return None


def save_jsonhash(jsonhash, mongoClient, database):
    """!

    @brief saves a hash of the json parameters file to the mongodb database
    @param jsonhash the data to be saved
    @param mongoClient the client to access the mongodb database
    @param database the database where we want to store jsonhash
    """
    try:
        mongoClient[database][params_collection].find_one_and_replace({'json_hash': {"$exists": True}}, {'json_hash': jsonhash}, upsert=True, return_document=True)
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))


def compare_jsonhash(new_jsonhash, mongoClient, database):
    """!
    @brief loads the json hash stored in the mongodb database and compares it to the current one to see if 
        it's an autorestart or not
    @param new_jsonhash the json hash to be compared with the one stored in database
    @param mongoClient the client to access the mongodb database
    @param database the database where the jsonhash is stored
    @return True if equal, False otherwise
    """
    try:
        hash = mongoClient[database][params_collection].find_one({'json_hash':{"$exists": True}})
        if hash != None and new_jsonhash == hash['json_hash']:
            return True
        return False
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))
        return False


def load_psi0(mongoClient, database):
    """!
    @brief loads one psi saved in the mongodb database
    @param mongClient the client to access the mongodb database
    @param database the database where the psi0 are stored
    @return the tuple (psi0, timestep) from the database or None if not found
    """
    try:
        psi0_cursor_rpart = mongoClient[database][processing_collection].find({'rpart': {"$exists": True}}).sort('timestep', -1).limit(1)
        psi0_cursor_ipart = mongoClient[database][processing_collection].find({'ipart': {"$exists": True}}).sort('timestep', -1).limit(1)
        psi0 = psa_solver.psi_mat(binary_to_numpy(psi0_cursor_rpart[0]['data']), binary_to_numpy(psi0_cursor_ipart[0]['data']))
        return (psi0, psi0_cursor_rpart[0]['timestep'])
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))
        return None


def load_all_psi(mongoClient, database, time_range = [-1, -1]):
    """!
        @brief loads the collection of psi's rparts and iparts from the database
        @param mongoClient the mongodb client to access the database
        @param database the database where to find psi
        @param time_range if left as default ([-1, -1]) it loads all the psis but otherwise it loads the psis that have a timestep within these bounds
        @return (cursor to rparts, cursor to iparts) , sorted in increasing timestep order, if no mongodb error has occured, None otherwise
        @warning the cursor points to the raw rparts and iparts stored in the database, so it's in binary and not numpy matrices.
            To do so you first need to call binary_to_numpy with the corresponding rpart and ipart you want to convert.
        @see binary_to_numpy
    """
    try:
        if time_range[0] == -1 and time_range[1] == -1:
            return mongoClient[database][processing_collection].find({'rpart':{"$exists": True}}).sort('timestep', 1), mongoClient[database][processing_collection].find({'ipart':{"$exists": True}}).sort('timestep', 1)
    
        return mongoClient[database][processing_collection].find({'rpart':{"$exists": True}, 'timestep': {"$gt": time_range[0], "$lt": time_range[1]}}).sort('timestep', 1), mongoClient[database][processing_collection].find({'ipart':{"$exists": True}, "timestep": {"$gt": time_range[0], "$lt": time_range[1]}}).sort('timestep', 1)    
    except pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))
        return None


class Initialization:
    """!
        @brief class used to store the parameters of the solver, handles the initialization part of the preprocessing via a user defined json file
    """    
    def __init__(self, json_file):
        """!
        @brief instantiate the class by declaring the parameters and initializing them
        @param json_file the json file to initialize the values from
        """
        self.jsonfile = json_file
        self.x_minmax = [0,0]
        self.y_minmax = [0,0]
        self.xnum = 0
        self.ynum = 0
        self.m = 0
        self.h_bar = 0
        self.method = ''
        
    
    def save_parameters(self, mongoClient, database):
        """!
            @brief saves the parameters that are relevant to post processing and monitoring.
            @note it saves V0, the method type (ftcs, ...), xmin/xmax, ymin/ymax, num of points along x axis, num of points along y axis, m, h_bar, timestep and totaltime.
            @param mongoClient the mongoDB client used to save the data
            @param database the database where to store the parameters
        """
        try:
            bin_v0 = numpy_to_binary(self.V0)
            param_id = mongoClient[database][params_collection].find_one_and_replace({'V0': {"$exists": True}}
            , {'V0': bin_v0, 
                'method': self.method,
                'xmin': self.x_minmax[0],
                'xmax': self.x_minmax[1],
                'ymin': self.y_minmax[0],
                'ymax': self.y_minmax[1],
                'xnum': self.xnum,
                'ynum': self.ynum,
                'm': self.m,
                'h_bar': self.h_bar,
                'totaltime': self.total_time,
                'timestep': self.dt
            }
            ,upsert=True, return_document=True)['_id']
            logger.debug("parameters saved, id: %s" % (str(param_id)))
        except pymongo.errors.OperationFailure as e:
            logger.error("MONGODB ERROR: %s" % (e))


    
    def V0try_init_withimage(self, mjson):
        """!
        @brief tries to initialize the potential V(x,y) = V0 with an image
        @param mjson the part of the json where to find the different
            paramaters for V0's values
        """
        if ('image' in mjson) and mjson['image']['is_path_defined'] == True:
            image = mjson['image']
            self.V_minmax = (image['vmin'], image['vmax'])
            self.V0 = get_potential_from_image(image['path_potential_init'], self.V_minmax[0], self.V_minmax[1])
            self.xnum = self.V0.shape[0]
            self.ynum = self.V0.shape[1]
            self.x = np.linspace(self.x_minmax[0], self.x_minmax[1], self.xnum)
            self.y = np.linspace(self.y_minmax[0], self.y_minmax[1], self.ynum)
    
    def V0try_init_withfunc(self, mjson):
        """!
        @brief tries to initialize the potential V(x,y) = V0 with a function
            which is either a constant value, the sum of squares or Acos(c_x * x + phi_x) + Bcos(c_y * y + phi_y)
        @param mjson the part of the json where to find the different
            paramaters for the function V0 is defined by   
        """
        if 'potential_function' in mjson:
            potentialfunc = mjson['potential_function']
            if 'constant' in potentialfunc:
                self.V0 = np.ones((len(self.x), len(self.y)), order='F') * potentialfunc['constant']
            if 'poly_deg_2' in potentialfunc:
                coefs = potentialfunc['poly_deg_2']['coef_list']
                offsets = potentialfunc['poly_deg_2']['offset_list']
                vx = coefs[0] * np.power(self.x - offsets[0], 2)
                vy = coefs[1] * np.power(self.y - offsets[1], 2)
                self.V0 = np.zeros((len(self.x), len(self.y)), order='F')
                for row in range(self.V0.shape[0]):
                        self.V0[row] = np.clip(vx[row] + vy, -1e10, 1e10)
            elif 'period' in potentialfunc:
                self.V0 = np.zeros((len(self.x), len(self.y)), order='F')
                phase_x = potentialfunc['period']['phase_x']                
                coef_x = potentialfunc['period']['coef_x']               
                offset_x = potentialfunc['period']['offset_x']           
                offset_y = potentialfunc['period']['offset_y']               
                coef_y = potentialfunc['period']['coef_y']
                phase_y = potentialfunc['period']['phase_y']

                vx = offset_x * np.cos(coef_x * self.x + phase_x)
                vy = offset_y * np.cos(coef_y * self.y + phase_y)
                for row in range(self.V0.shape[0]):
                    self.V0[row] = vx[row] + vy


    
    def try_init_params(self, mjson):
        """!
        @brief tries to initialise as much parameters as it can
            via a json object
        @param mjson the json object where to find the parameters     
        """
        if 'method' in mjson:
            self.method = mjson['method']
        if 'parameters' in mjson:
            params = mjson['parameters']
            if 'timestep' in params:
                self.dt = params['timestep']
            if 'totaltime' in params:
                self.total_time = params['totaltime']
            if 'xmin' in params:
                self.x_minmax[0] = params['xmin']
            if 'xmax' in params:
                self.x_minmax[1] = params['xmax']
            if 'ymin' in params:
                self.y_minmax[0] = params['ymin']
            if 'ymax' in params:
                self.y_minmax[1] = params['ymax']
            if 'm' in params:
                self.m = params['m']
            if 'h_bar' in params:
                self.h_bar = params['h_bar']
            if 'x_num_point' in params:
                self.xnum = params['x_num_point']
            if 'y_num_point' in params:
                self.ynum = params['y_num_point']
            #will [xy]_minmax will be initialized via the default json
            #same for [xy]num
            self.x = np.linspace(self.x_minmax[0], self.x_minmax[1], self.xnum)
            self.y = np.linspace(self.y_minmax[0], self.y_minmax[1], self.ynum)
            self.psi0 = psa_solver.psi_mat(np.zeros((len(self.x), len(self.y)), dtype=np.double, order='F'), np.zeros((len(self.x), len(self.y)), dtype=np.double, order='F'))

    

    
    def get_gaussian(self, json_gaussian):
        """!
        @brief adds a gaussian wave packet to psi0
        
        @param json_gaussian the json object where to find the gaussian wave packet's parameters
        """
        A = json_gaussian['norm_constant']
        x0 = json_gaussian['x0']
        y0 = json_gaussian['y0']
        kx = json_gaussian['init_velocity_x']
        ky = json_gaussian['init_velocity_y']
        w = json_gaussian['width']
        weight = json_gaussian['weight']
        psix = weight * A * np.exp(-np.power(self.x - x0, 2)) / (w*w)
        psi_buff_rpart = np.zeros_like(self.psi0.rpart, order='F')
        psi_buff_ipart = np.zeros_like(self.psi0.ipart, order='F')
        for row in range(len(self.x)):
            psi_buff_rpart[row] += psix[row] * np.exp(-np.power(self.y - y0, 2)) * np.cos(ky * self.y + kx * self.x[row])
            psi_buff_ipart[row] += psix[row] * np.exp(-np.power(self.y - y0, 2)) * np.sin(ky * self.y + kx * self.x[row])

        self.psi0.rpart = psi_buff_rpart
        self.psi0.ipart = psi_buff_ipart

    
    def compute_psi0(self, json_oscillator):
        """!
        @brief computes the 2D HO solutions at time t = 0
        @return the cube representing psi0(x, y)
        """
        n_x = json_oscillator['nx']
        n_y = json_oscillator['ny']
        w = json_oscillator['omega']
        weight = json_oscillator['weight']
        H_x = sp.hermite(n_x)
        H_y = sp.hermite(n_y)
        factor_pow = (math.pow(self.m*w/(np.pi * self.h_bar), 0.25))
        factor_mw = self.m*w/self.h_bar

        psi_nx = weight * (1./math.sqrt(math.pow(2., n_x)*math.factorial(n_x))) * factor_pow * np.exp((-factor_mw / 2.) * np.power(self.x, 2)) * H_x(math.sqrt(factor_mw) * self.x)
        psi_ny = (1./math.sqrt(math.pow(2., n_y)*math.factorial(n_y))) * factor_pow * np.exp((-factor_mw / 2.) * np.power(self.y, 2)) * H_y(math.sqrt(factor_mw) * self.y) 
        psi_buff = np.zeros_like(self.psi0.rpart, order='F')
        for row in range(len(self.x)):
            psi_buff[row] += psi_nx[row] * psi_ny
            
        self.psi0.rpart = psi_buff

    
    def psi0try_init(self, mjson):
        """!
        @brief initializes psi0 with either a gaussian wave or the 
            solution to a 2D-Harmonic Oscillator
        @param mjson the part of the json where to find the different
            paramaters for the psi0
        """
        if 'psi_function' in mjson:
            psi_func = mjson['psi_function']
            if 'gaussian' in psi_func:
                for gaussian in psi_func['gaussian']:
                    self.get_gaussian(gaussian)
            elif 'oscillator' in psi_func:
                for oscillator in psi_func['oscillator']:
                    self.compute_psi0(oscillator)

    
    def init_lightParamsOnly(self):
        """!
        @brief initializes light parameters only, that is everything except the matrices psi0 and v0
        @param mjson the  json object where to find all the parameters needed to 
            do the simulation
        """
        #first load the default json file provided by the developers
        with open('../json/start_file.json', 'r') as file:
            mjson = json.load(file)
            self.try_init_params(mjson)
        #then read the user-defined json file
        with open(self.jsonfile, 'r') as file:
            mjson = json.load(file)
            self.try_init_params(mjson)
            self.dx = self.x[1] - self.x[0]
            self.dy = self.y[1] - self.y[0]

    
    def init_viajson(self, mjson):
        """!
        @brief initializes all parameters of the object using a json object
        @param mjson the  json object where to find all the parameters needed to 
            do the simulation
        """
        self.try_init_params(mjson)
        self.V0try_init_withimage(mjson)
        self.V0try_init_withfunc(mjson)
        self.psi0try_init(mjson)

    
    def init_values(self):
        """!
        @brief initializes all the parameters needed to do the simulation
            with first the default json file and then again but with the user-defined
            json file which will potentially override the parameters provided in the default json file.
        """
        #first load the default json file provided by the developers
        with open('../json/start_file.json', 'r') as file:
            mjson = json.load(file)
            self.init_viajson(mjson)
        #then read the user-defined json file
        with open(self.jsonfile, 'r') as file:
            mjson = json.load(file)
            self.init_viajson(mjson)
        self.dx = self.x[1] - self.x[0]
        self.dy = self.y[1] - self.y[0]
        