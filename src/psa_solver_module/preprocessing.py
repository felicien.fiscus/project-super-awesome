#!/usr/bin/env python3
"""!
    @file preprocessing.py
    @brief backend functions for autorestart mechanism and cleaning up the database
"""

from psa_solver import init_and_utilities as iu
from matplotlib import pyplot as plt
from init_and_utilities import logger
import hashlib


def testMatplotlib(init_obj):
    """!
        @brief for testing only, vizualisation of psi0 and V0 with matplotlib
        @param ini_obj the initialization object to be tested
    """
    X, Y = iu.np.meshgrid(init_obj.x, init_obj.y)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(X, Y, iu.np.absolute(init_obj.psi0.rpart + init_obj.psi0.ipart * 1j), rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
    ax.set_zlim(-1.01, 1.01)
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111, projection='3d')
    surf1 = ax1.plot_surface(X, Y, init_obj.V0, rstride=1, cstride=1, cmap='hot', linewidth=0, antialiased=False)
    ax1.set_zlim(-10.01, 10.01)

    cbar_fig = fig.colorbar(surf, shrink=0.5, aspect=5, ax=ax)
    cbar_fig1 = fig1.colorbar(surf, shrink=0.5, aspect=5, ax=ax1)
    cbar_fig.ax.get_yaxis().labelpad = 15
    cbar_fig1.ax.get_yaxis().labelpad = 15
    cbar_fig.ax.set_ylabel('densité de probabilité', rotation=270)
    cbar_fig1.ax.set_ylabel('kg.m^2.s^-2', rotation=270)

    plt.show()


def get_jsonhash(jsonfile):
    """!
        @brief hashes (using md5) the json stored in a file and returns it
        @param jsonfile the file storing the json to be hashed
        @return the hash of the json object
    """
    with open(jsonfile, 'r') as mjson:
        mjson_tobehashed = iu.json.dumps(iu.json.load(mjson), sort_keys=True, indent=2)
        json_hash = hashlib.md5(mjson_tobehashed.encode("utf-8")).hexdigest()
        return json_hash
    

def try_autorestart(init_obj, mongoClient, database):
    """!
        @brief tests if the json given as an input to the init object is the same as the one given last execution
            and if so loads  psi0 from the database rather than from the json parameters
        @param ini_obj the Initialization object already constructed
        @param mongoClient the mongoDB client from which to load psi0
        @param database from which to load psi0
        @return the timestep at which to start the computations (!= 0.0 if autorestart)
    """
    jsonhash = get_jsonhash(init_obj.jsonfile)
    if iu.compare_jsonhash(jsonhash, mongoClient, database):
        logger.debug('same json hash, proceeding to load psi0 from database')
        psi0, timestep = iu.load_psi0(mongoClient, database)
        init_obj.init_lightParamsOnly()
        logger.debug('timestep loaded: %f time total: %f' % (timestep, init_obj.total_time))
        if timestep >= init_obj.total_time:
            logger.debug('Simulation finished, stopping simulation')
            exit(0)
        else:
            init_obj.psi0 = psi0
            init_obj.V0 = iu.load_param('V0', mongoClient, database)
            return timestep
    else:
        logger.debug('different json hash, cleaning up database and saving new hash')
        cleanup_database(mongoClient, database)
        iu.save_jsonhash(jsonhash, mongoClient, database)
        init_obj.init_values()
        init_obj.save_parameters(mongoClient, database)
        return  0.0





def init_solver(json_file, mongoDBinfo):
    """!
        @brief creates an Initialization object with a json file and tries to do autorestart
        @note Initialization object is responsible for holding the values used by the solver at initialization
        @param json_file the file in which to find the init parameters
        @return an Initialization object and the begin time (!= 0 if there has been an autorestart)
    """
    mongoClient = iu.open_connection(mongoDBinfo.user, mongoDBinfo.pwd, mongoDBinfo.ip, mongoDBinfo.db)
    init = iu.Initialization(json_file)
    begin_time = try_autorestart(init, mongoClient, mongoDBinfo.db)
    mongoClient.close()
    return init, begin_time


def cleanup_database(mongoClient, database):
    """!
        @brief cleans the database up by deleting all the instances of psi0 stored and all the instances of json hashes
        @param mongoClient the mongoDB client from which to load psi0
        @param database where to store / load the data
    """
    try:
        mongoClient[database][iu.processing_collection].delete_many({})
        mongoClient[database][iu.params_collection].delete_many({})
        mongoClient[database][iu.norm_error_collection].delete_many({})
    except iu.pymongo.errors.OperationFailure as e:
        logger.error("MONGODB ERROR: %s" % (e))