#!/usr/bin/env python3
"""!
    @file postprocessing.py
    @brief usage: ./postprocessing.py word jsonfile
        where jsonfile is the json containing the initial parameter of the solver and
        where word is either :
                - 'e' to generate the graph of the current normalisation error since the beginning of computation
                - 'i' to generate a vtk image of the initial state of the solver
                - 'l' to generate the vtk image of the last psi computed by the solver
                - 'a' to generate the vtk images of all the psis computed by solver so far
                - 'm' to modify the parameters used by an ongoing calculation that has been stopped
                - 'ao' to generate the vtk images of all the psis computed by solver so far using mutliprocessing (faster)
                - 'anim' to generate the minimum amount of vtk images of the psis to have a 30fps animation of the whole thing so far 
                - 'animo' like 'anim' but with multiprocessing (faster)
    @note in the case of 'anim' or 'animo', you can replace the jsonfile option with the number of fps you want the animation to be.
"""

from psa_solver import init_and_utilities as iu
from psa_solver import psi_mat
import sys
import pyevtk as pvtk
from colorama import Fore
from psa_solver import preprocessing as pproc
import multiprocessing as multi
import time
from matplotlib import pyplot as plt

#maximum 8 process in parallel
process_num = multi.Value('i', 8)
#to evaluate the progress of the generation of vtk images
progress_num = multi.Value('i', 0, lock=False)
progress_accu = multi.Value('f', 0.0, lock=False)
progress_lock = multi.Lock()

debug_info = ''


def get_last_psi(mongoClient, mongoInfo):
    """!
        @brief generates the vtk_image of the last psi computed by the solver
        @param mongoClient the client to the mongo database containing the computed psis
        @param mongoInfo information concerning the mongoDB database
    """
    global debug_info
    V0 = iu.load_param('V0', mongoClient, mongoInfo.db)
    psi, timestep = iu.load_psi0(mongoClient, mongoInfo.db)
    psi_module = iu.np.sqrt(iu.np.power(psi.rpart, 2) + iu.np.power(psi.ipart, 2))
    pvtk.hl.imageToVTK("vtk_images/psi_image_%06d" % (timestep * 1e6), pointData=
    {
        'psi_module': psi_module.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
        , 'psi_rpart': psi.rpart.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
        , 'psi_ipart': psi.ipart.astype(iu.np.float32).reshape((psi.ipart.shape[0], psi.ipart.shape[1], 1), order='C')
        , 'V0': V0.astype(iu.np.float32).reshape((V0.shape[0], V0.shape[1], 1), order='C')
    })
    iu.logger.debug(debug_info + Fore.GREEN + 'last psi vtk image generated' + Fore.RESET)


def get_init_image(jsonfile):
    """!
        @brief generates the vtk image of the initial state of the solver and saves it to the database
        @param jsonfile the json containing the parameters of the initial state 
        @param mongoClient the client to the mongo database containing the computed psis
        @param mongoInfo information concerning the mongoDB database
    """
    global debug_info
    initObj = iu.Initialization(jsonfile)
    initObj.init_values()
    psi = initObj.psi0
    psi_module = iu.np.sqrt(iu.np.power(psi.rpart, 2) + iu.np.power(psi.ipart, 2))
    pvtk.hl.imageToVTK("vtk_images/psi_image_0", pointData=
    {
        'psi_module': psi_module.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
        , 'psi_rpart': psi.rpart.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
        , 'psi_ipart': psi.ipart.astype(iu.np.float32).reshape((psi.ipart.shape[0], psi.ipart.shape[1], 1), order='C')
        , 'V0': initObj.V0.astype(iu.np.float32).reshape((initObj.V0.shape[0], initObj.V0.shape[1], 1), order='C')
    })
    iu.logger.debug(debug_info + Fore.GREEN + 'psi vtk image generated' + Fore.RESET)


def get_all_psis(mongoClient, mongoInfo):
    """!
        @brief generates the vtk images of all the psis computed by the solver
        @param mongoClient the client to the mongo database containing the computed psis
        @param mongoInfo information concerning the mongoDB database
    """
    global debug_info
    V0 = iu.load_param('V0', mongoClient, mongoInfo.db)
    rparts, iparts = iu.load_all_psi(mongoClient, mongoInfo.db)
    #there's the same number of rparts than iparts
    nb_of_psi = mongoClient[mongoInfo.db][iu.processing_collection].count_documents({"rpart": {"$exists": True}})
    i = 0
    for (rpart, ipart) in zip(rparts, iparts):
        psi = psi_mat(iu.binary_to_numpy(rpart['data']), iu.binary_to_numpy(ipart['data']))
        timestep = rpart['timestep']
        psi_module = iu.np.sqrt(iu.np.power(psi.rpart, 2) + iu.np.power(psi.ipart, 2))
        pvtk.hl.imageToVTK("vtk_images/psi_image_%06d" % (timestep * 1e6), pointData=
        {
            'psi_module': psi_module.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_rpart': psi.rpart.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_ipart': psi.ipart.astype(iu.np.float32).reshape((psi.ipart.shape[0], psi.ipart.shape[1], 1), order='C')
            , 'V0': V0.astype(iu.np.float32).reshape((V0.shape[0], V0.shape[1], 1), order='C')
        })
        if i % 100 == 0:
            iu.logger.debug(debug_info + Fore.YELLOW + ('%d/%d' % (i, nb_of_psi-1)) + Fore.RESET + ' vtk images processed : ' + Fore.YELLOW + ('%d%%' % (100.0 * float(i) / float(nb_of_psi)) + Fore.RESET))   
        i += 1
    
    iu.logger.debug(debug_info + Fore.GREEN + ('%d/%d' % (i-1, nb_of_psi-1)) + Fore.RESET + ' vtk images processed : ' + Fore.GREEN + ('%d%%' % (100.0 * float(i) / float(nb_of_psi)) + Fore.RESET))


def get_all_psis_optimized(mongoClient, mongoInfo, rank, V0, totaltime):
    """!
        @brief generates the vtk images of all the psis computed by the solver but it's parallel programming
        @param mongoClient the client to the mongo database containing the computed psis
        @param mongoInfo information concerning the mongoDB database
        @param rank the rank of the process executing this function
        @param V0 the potential matrix
        @param totaltime time of the simulation
    """
    global debug_info
    rparts, iparts = iu.load_all_psi(mongoClient, mongoInfo.db, time_range=[rank * totaltime / process_num.value, (rank + 1) * totaltime / process_num.value])
    #there's the same number of rparts than iparts
    nb_of_psi = mongoClient[mongoInfo.db][iu.processing_collection].count_documents({"rpart": {"$exists": True}})
    for (rpart, ipart) in zip(rparts, iparts):
        psi = psi_mat(iu.binary_to_numpy(rpart['data']), iu.binary_to_numpy(ipart['data']))
        timestep = rpart['timestep']
        psi_module = iu.np.sqrt(iu.np.power(psi.rpart, 2) + iu.np.power(psi.ipart, 2))
        pvtk.hl.imageToVTK("vtk_images/psi_image_%06d" % (timestep * 1e6), pointData=
        {
            'psi_module': psi_module.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_rpart': psi.rpart.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_ipart': psi.ipart.astype(iu.np.float32).reshape((psi.ipart.shape[0], psi.ipart.shape[1], 1), order='C')
            , 'V0': V0.astype(iu.np.float32).reshape((V0.shape[0], V0.shape[1], 1), order='C')
        })
        if progress_num.value % 100 == 0:
            iu.logger.debug(debug_info + Fore.YELLOW + ('%d/%d' % (progress_num.value, nb_of_psi-1)) + Fore.RESET + ' vtk images processed : ' + Fore.YELLOW + ('%d%%' % (100.0 * float(progress_num.value) / float(nb_of_psi)) + Fore.RESET))   
        progress_lock.acquire()
        progress_num.value += 1
        progress_lock.release()
    
    iu.logger.debug(debug_info + Fore.YELLOW + ('%d/%d' % (progress_num.value, nb_of_psi-1)) + Fore.RESET + ' vtk images processed : ' + Fore.YELLOW + ('%d%%' % (100.0 * float(progress_num.value) / float(nb_of_psi)) + Fore.RESET))


def modify_onGoing(jsonfile, mongoClient, mongoInfo):
    """!
        @brief change the parameters used by an ongoing calculation that has been stopped
        @param jsonfile the json containing the new parameters
        @param mongoClient the client to the mongoDB database where to store the parameters
        @param mongoInfo the informations about the mongoDB database

        @warning do not modify the initial mesh parameters or it will potentially by a catastrophe !
    """
    global debug_info
    init = iu.Initialization(jsonfile)
    init.init_values()
    init.save_parameters(mongoClient, mongoInfo.db)
    method, dt, totaltime = iu.load_param('method', mongoClient, mongoInfo.db), iu.load_param('timestep', mongoClient, mongoInfo.db), iu.load_param('totaltime', mongoClient, mongoInfo.db)
    debug_info = 'method: %s | timestep: %fms | totaltime: %fs | ' % (method, dt*1e3, totaltime)
    #save the jsonhash so that the solver will automatically restart from where it has stopped
    hash = pproc.get_jsonhash(jsonfile)
    iu.save_jsonhash(hash, mongoClient, mongoInfo.db)
    iu.logger.debug(debug_info + Fore.GREEN + 'Saved new jsonhash, the calculation is good to go !' + Fore.RESET)



keep_ploting = True

  
def display_error(mongoClient, mongoInfo):
    """!
        @brief displays the error rate as a curve using matplotlib
        @param mongoClient the client to the mongodb database to load the errors values from
        @param mongoInfo the information about that database (name, address, etc.)
    """ 
    method = iu.load_param('method', mongoClient, mongoInfo.db)
    def key_event(event):
        if event.key == 'escape':
            global keep_ploting
            keep_ploting = False
    while keep_ploting:
        norm_errors, timesteps = iu.load_all_errors(mongoClient, mongoInfo.db)
        plt.plot(timesteps, norm_errors)
        plt.xlabel('time (s)')
        plt.ylabel('normalization error')
        plt.title(method + ' method').set_color(color='blue')
        plt.pause(0.05)    
        
        fig = plt.gcf()  # get current figure
        fig.clear()
        fig.canvas.mpl_connect('key_press_event', key_event)
        

def generate_animation(mongoClient, mongoInfo, fps):
    """!
        @brief generates only the vtk images required to make an animation of the whole simulation
        @param mongoClient the client to the mongodb database to load the errors values from
        @param mongoInfo the information about that database (name, address, etc.)
        @param fps the rate of image to be generated for one second of animation
    """
    totaltime = iu.load_param('totaltime', mongoClient, mongoInfo.db)
    dt2 = iu.load_param('timestep', mongoClient, mongoInfo.db) * 0.5
    V0 = iu.load_param('V0', mongoClient, mongoInfo.db)
    accu = 0.0
    accu_step = 1.0 / fps
    i = 0

    while accu <= totaltime:
        rparts, iparts = iu.load_all_psi(mongoClient, mongoInfo.db, time_range=[accu - dt2, accu + dt2])
        rpart, ipart = rparts[0], iparts[0]
        psi = psi_mat(iu.binary_to_numpy(rpart['data']), iu.binary_to_numpy(ipart['data']))
        timestep = rpart['timestep']
        psi_module = iu.np.sqrt(iu.np.power(psi.rpart, 2) + iu.np.power(psi.ipart, 2))
        pvtk.hl.imageToVTK("vtk_images/psi_image_%06d" % (timestep * 1e6), pointData=
        {
            'psi_module': psi_module.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_rpart': psi.rpart.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_ipart': psi.ipart.astype(iu.np.float32).reshape((psi.ipart.shape[0], psi.ipart.shape[1], 1), order='C')
            , 'V0': V0.astype(iu.np.float32).reshape((V0.shape[0], V0.shape[1], 1), order='C')
        })
        #every 5 seconds
        if i % 50 == 0:
            iu.logger.debug(debug_info + Fore.YELLOW + ('%d/%ds' % (accu, totaltime)) + Fore.RESET + ' vtk images processed : ' + Fore.YELLOW + ('%d%%' % (100.0 * accu / totaltime)) + Fore.RESET)

        i += 1
        accu += accu_step

    iu.logger.debug(debug_info + Fore.GREEN + ('%d/%ds' % (accu, totaltime)) + Fore.RESET + ' vtk images processed : ' + Fore.GREEN + ('%d%%' % (100.0 * accu / totaltime) + Fore.RESET))


def generate_animation_multiproc(mongoClient, mongoInfo, fps, rank, totaltime, dt2, V0):
    """!
        @brief generates only the vtk images required to make an animation of the whole simulation but using parallelization
        @param mongoClient the client to the mongodb database to load the errors values from
        @param mongoInfo the information about that database (name, address, etc.)
        @param rank the rank of the process executing this function
        @param fps the rate of image to be generated for one second of animation
        @param totaltime time of the simulation
        @param dt2 the timestep between each psi computed divided by 2
        @param V0 the potential matrix
    """
    accu = rank * (totaltime / process_num.value)
    accu_step = 1.0 / fps
    i = 0

    while accu < (rank + 1.0) * (totaltime / process_num.value):
        rparts, iparts = iu.load_all_psi(mongoClient, mongoInfo.db, time_range=[accu - dt2, accu + dt2])
        rpart, ipart = rparts[0], iparts[0]
        psi = psi_mat(iu.binary_to_numpy(rpart['data']), iu.binary_to_numpy(ipart['data']))
        timestep = rpart['timestep']
        psi_module = iu.np.sqrt(iu.np.power(psi.rpart, 2) + iu.np.power(psi.ipart, 2))
        pvtk.hl.imageToVTK("vtk_images/psi_image_%06d" % (timestep * 1e6), pointData=
        {
            'psi_module': psi_module.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_rpart': psi.rpart.astype(iu.np.float32).reshape((psi.rpart.shape[0], psi.rpart.shape[1], 1), order='C')
            , 'psi_ipart': psi.ipart.astype(iu.np.float32).reshape((psi.ipart.shape[0], psi.ipart.shape[1], 1), order='C')
            , 'V0': V0.astype(iu.np.float32).reshape((V0.shape[0], V0.shape[1], 1), order='C')
        })
        #every 5 seconds
        if i % 50 == 0:
            iu.logger.debug(debug_info + Fore.YELLOW + ('%d/%ds' % (progress_accu.value, totaltime)) + Fore.RESET + ' vtk images processed : ' + Fore.YELLOW + ('%d%%' % (100.0 * progress_accu.value / totaltime)) + Fore.RESET)

        i += 1
        accu += accu_step
        progress_lock.acquire()
        progress_accu.value += accu_step
        progress_lock.release()

    iu.logger.debug(debug_info + Fore.GREEN + ('%d/%ds' % (progress_accu.value, totaltime)) + Fore.RESET + ' vtk images processed : ' + Fore.GREEN + ('%d%%' % (100.0 * progress_accu.value / totaltime) + Fore.RESET))


def main():
    mongoInfo = iu.mongoInfo()
    if len(sys.argv) > 1:
        jsonfile = '../json/file_user.json'
        if len(sys.argv) > 2 and sys.argv[1] != 'anim' and sys.argv[1] != 'animo':
            jsonfile = sys.argv[2]
            mongoInfo.init_via_json(jsonfile)
        global debug_info
        mongoClient = iu.open_connection(mongoInfo.user, mongoInfo.pwd, mongoInfo.ip, mongoInfo.db)
        method, dt, totaltime = iu.load_param('method', mongoClient, mongoInfo.db), iu.load_param('timestep', mongoClient, mongoInfo.db), iu.load_param('totaltime', mongoClient, mongoInfo.db)
        debug_info = 'method: %s | timestep: %fms | totaltime: %fs | ' % (method, dt*1e3, totaltime)
        run_option = sys.argv[1]
        if run_option == 'i':
            get_init_image(jsonfile)
        elif run_option == 'l':
            get_last_psi(mongoClient, mongoInfo)
        elif run_option == 'a':
            time_taken = time.time()
            get_all_psis(mongoClient, mongoInfo)
            iu.logger.debug(('time taken: ' + Fore.BLUE + '%01f' + ' seconds' + Fore.RESET) % ((time.time() - time_taken)))
        elif run_option == 'm':
            modify_onGoing(jsonfile, mongoClient, mongoInfo)
        elif run_option == 'ao':
            time_taken = time.time()
            V0 = iu.load_param('V0', mongoClient, mongoInfo.db)
            processes = [ multi.Process(name= 'THREAD-%01d' % (i), target = get_all_psis_optimized, args= (mongoClient, mongoInfo, i, V0, totaltime)) for i in range(process_num.value) ]
            
            for i in range(len(processes)):
                processes[i].start()

            for i in range(len(processes)):
                processes[i].join()
                    
            iu.logger.debug(debug_info + Fore.GREEN + 'vtk images have been processed' + Fore.RESET)
            iu.logger.debug(('time taken: ' + Fore.BLUE + '%01f' + ' seconds' + Fore.RESET) % ((time.time() - time_taken)))
        elif run_option == 'e':
            display_error(mongoClient, mongoInfo)
        elif run_option == 'anim':
            fps = 30
            if len(sys.argv) > 2:
                fps = int(sys.argv[2])
            
            iu.logger.debug('using fps = %i' % (fps))
            generate_animation(mongoClient, mongoInfo, fps)
        elif run_option == 'animo':
            fps = 30
            if len(sys.argv) > 2:
                fps = int(sys.argv[2])
            
            iu.logger.debug('using fps = %i' % (fps))
            time_taken = time.time()
            totaltime = iu.load_param('totaltime', mongoClient, mongoInfo.db)
            dt2 = iu.load_param('timestep', mongoClient, mongoInfo.db) * 0.5
            V0 = iu.load_param('V0', mongoClient, mongoInfo.db)
            processes = [ multi.Process(name= 'THREAD-%01d' % (i), target = generate_animation_multiproc, args= (mongoClient, mongoInfo, fps, i, totaltime, dt2, V0)) for i in range(process_num.value) ]
            
            for i in range(len(processes)):
                processes[i].start()

            for i in range(len(processes)):
                processes[i].join()
                    
            iu.logger.debug(debug_info + Fore.GREEN + 'vtk images have been processed' + Fore.RESET)
            iu.logger.debug(('time taken: ' + Fore.BLUE + '%01f' + ' seconds' + Fore.RESET) % ((time.time() - time_taken)))

        else:
            iu.logger.error('Unknown option passed as argument 1 of postprocessing.py')

        mongoClient.close()
    else:
        iu.logger.error('No option passed in CLI for postprocessing.py')



    



if __name__ == '__main__':
    main()